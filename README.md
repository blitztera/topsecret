# JSON Fetcher
Powered by:

[![N|Solid](https://scontent.fmnl13-1.fna.fbcdn.net/v/t1.0-9/89550700_104053414560154_5202011204335173632_n.jpg?_nc_cat=101&_nc_sid=e007fa&_nc_oc=AQnTUH8oQ8LLO-rJMjoq8qK2TT2UfwgGpbQm_SFgRmsDyExlrSzIrdQ0s1FhryByrrU&_nc_ht=scontent.fmnl13-1.fna&oh=eff9def63af7fee2158d18b2ffcf7836&oe=5E934D20)](http://wordpress.com/)

JSON Fetcher Plugin to fetch data asynchronously using VUE.


### Tech

JSON Fetcher stacks

* [WordPress](http://wordpress.com/) - HTML enhanced for web apps!
* [Vue.js](https://vuejs.org/) - awesome web-based text editor
* [Twitter Bootstrap] - great UI boilerplate for modern web apps

### Installation
1. Download [Composer](https://getcomposer.org/download/)
2. Clone the JSON Fetcher BitBucket Repository at https://blitztera@bitbucket.org/blitztera/topsecret.git

You can also choose to setup XAMPP and WordPress locally
JSON Fetcher requires [WordPress](https://wordpress.org/latest.zip) v5.3+ and [XAMPP](https://www.apachefriends.org/download.html) v7.3+ to run.
[Git](https://git-scm.com/downloads) for windows will also be nice to have, so download it as well and install.
Install the dependencies and devDependencies and start the server.

### Usage
 Activate the plugin. Once activated, you will see JSON Fetcher in the menu

![N|Solid](https://scontent.fmnl13-1.fna.fbcdn.net/v/t1.0-9/89533991_104066957892133_3464386170744471552_n.jpg?_nc_cat=103&_nc_sid=e007fa&_nc_oc=AQngHEWMDuUmJP3y5Z0YCIGuYbjat4MN_Jjo6qMIAwZXDLsh5blTufq-M7xHjKiXcRs&_nc_ht=scontent.fmnl13-1.fna&oh=23a90f9a1c0a58e409606363d9e15294&oe=5E929935)

Click on JSON Fetcher and choose the user that you want to view. You can click on either their ID, Name or Username ![N|Solid](https://scontent.fmnl13-1.fna.fbcdn.net/v/t1.0-9/89722656_104066987892130_4501699366335021056_o.jpg?_nc_cat=104&_nc_sid=e007fa&_nc_oc=AQlwJ8rtDxAveXNtZV5V3cNPEJMV00Y61glbQb1zjQz3_jwzGhRaj6FaZvICFcyO9sE&_nc_ht=scontent.fmnl13-1.fna&oh=db1563f09e1bdc2e5e76296c7d934923&oe=5E9282D6)

After you clicked, data will be shown. You can check other users too!![N|Solid](https://scontent.fmnl13-1.fna.fbcdn.net/v/t1.0-9/87339519_104066971225465_3418257659344715776_o.jpg?_nc_cat=110&_nc_sid=e007fa&_nc_oc=AQnM03DDX2e5t07hoFJ-asADRrCCP3VpHGOWSqlRdc0KHt7hkGo0vPrf7SV4kWBk7DU&_nc_ht=scontent.fmnl13-1.fna&oh=7455417aac18efdd1db56baa64d8f43b&oe=5E910EFE)


License
----

GPL2

