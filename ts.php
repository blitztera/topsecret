<?php
/*
Plugin Name: JSON Fetcher
Plugin URI:  plugin url
Description: Plugin to fetch data asynchronously using VUE.
Version:     1.0
Author:      Tera Blitz
Author URI:  https://twitter.com/TeraBlitzer
License:     GPL2
*/

// Create a menu for JSON Fetcher
add_action("admin_menu", "addMenu");
function addMenu()
{
  add_menu_page("JSON Fetcher", "JSON Fetcher", 4, "fetch", "exampleMenu" );
  add_submenu_page("example_options", "Option 1", "Option 1", 4, "example-option-1", "option1");
}

function exampleMenu()
{
include( plugin_dir_path( __FILE__ ) . 'fetch.html');
} 

?>
