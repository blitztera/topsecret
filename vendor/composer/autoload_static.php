<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInita6f0ac355a56f3868b5bbb22f9419cfa
{
    public static $prefixLengthsPsr4 = array (
        'C' => 
        array (
            'Composer\\Installers\\' => 20,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Composer\\Installers\\' => 
        array (
            0 => __DIR__ . '/..' . '/composer/installers/src/Composer/Installers',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInita6f0ac355a56f3868b5bbb22f9419cfa::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInita6f0ac355a56f3868b5bbb22f9419cfa::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
